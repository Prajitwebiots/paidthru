var nodemailer = require('nodemailer');
var ejs = require('ejs');

// import config from '../config/config';
// import fs from 'fs';

// create reusable transporter object using the default SMTP transport
// const transporter = nodemailer.createTransport({
//   service: 'localhost',
//   port: 25,
//   secure: false,
//   ignoreTLS: true,
//   debug: true,
// });

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'testproject342@gmail.com',
        pass: 'testWebiots@342'
    }
});


const Email = {

    sendActivationEmail: (body, token) => {
        var link = global.url + '/activation/' + body.email + '/' + token;
        ejs.renderFile("./views/emails/activation.ejs", { username: body.username, link: link, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Account Activation', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendForgotPassworEmail: (body, token) => {
        var link = global.url + '/reset/' + token;
        ejs.renderFile("./views/emails/forgotpassword.ejs", { username: body.username, link: link, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Reset Password', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendAccountActiveEmail: (body) => {
        ejs.renderFile("./views/emails/accountactive.ejs", { username: body.username, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Account Activate', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendAccountBloackEmail: (body) => {
        ejs.renderFile("./views/emails/accountblock.ejs", { username: body.username, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Account Block', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendRejectWithdrawalEmail: (body, coin, amount) => {
        ejs.renderFile("./views/emails/rejectwithdrawal.ejs", { username: body.username, coin: coin, amount: amount, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Withdrawal Reject', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendDepositEmail: (body, coin, amount) => {
        ejs.renderFile("./views/emails/depositemail.ejs", { username: body.username, coin: coin, amount: amount, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'Deposit Amount', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendKycApprovedEmail: (body) => {
        ejs.renderFile("./views/emails/kycapproved.ejs", { username: body.username,  path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'KYC Approved', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendKycRejectEmail: (body) => {
        ejs.renderFile("./views/emails/kycreject.ejs", { username: body.username, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: body.email, // list of receivers
                    subject: 'KYC Rejected', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },

    sendKycForAdminEmail: (body) => {
        ejs.renderFile("./views/emails/kycadmin.ejs", { username: body.username, path: global.url },
            function(err, data) {
                console.log(err);
                var mailOptions = {
                    from: '"PaidThru" <paidthru@gmail.com>',
                    to: 'paidthru@gmail.com', // list of receivers
                    subject: 'KYC Verification', // Subject line
                    html: data
                };
                transporter.sendMail(mailOptions, function(error, info) {
                    if (error) {
                        console.log(error);
                    } else {
                        //console.log('Email sent: ' + info.response);
                    }
                })
            });
    },
};




module.exports = Email;