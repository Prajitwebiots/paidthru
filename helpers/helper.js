var bCrypt = require('bcryptjs');

const Helper = {

    // Generate Token
    randomString: (length) => {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = length;
        var randomstring = '';
        for (var i = 0; i < length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;
    },

    // Generate Bcrypt Password
    bcrypt: (password) => {
        console.log(password);
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    },

    // Check ERC20 Address
    isAddress: (address) => {
        const myregexp = /^(0x)?[0-9a-f]{40}$/i;
        const match = myregexp.exec(address);
        if (match == null) {
            return false;
        } else {
            const myregexp1 = /^(0x)?[0-9a-f]{40}$/;
            const match1 = myregexp1.exec(address);
            const myregexp2 = /^(0x)?[0-9a-fA-F]{40}$/;
            const match2 = myregexp2.exec(address);
            if (match1 == null || match2 != null) {
                return true;
            }
        }
    },


};


module.exports = Helper;