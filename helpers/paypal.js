"use_strict";
 
/* 
* Creating Nodejs PayPal Integration application
* @author Shashank Tiwari
*/
 
const paypal = require('paypal-rest-sdk');
var Deposit = require('../models/deposit');
// paypal auth configuration
var config = {
  "port" : 5000,
  "api" : {
    "host" : "api.sandbox.paypal.com",
    "port" : "",            
    "client_id" : "AWnP-a6IJEetsEavoAqg0Gw2VAIdT3IaRhlXExsVS1gP2LfG31D2aPSrytnAmzPU9WmZdMLPBEU013-M",  // your paypal application client id
    "client_secret" : "EI6CxZoyHL8bZ4W1x1WhLcW0hyEGOzlxPV9H4fNBi0EJINngfPqzGSBE7wRypqhOHXsMxAoAj2QCoh7E" // your paypal application secret id
  }
}
paypal.configure(config.api);

// create Deposit
const createDatabaseDeposit = (userData, currency, amount) => new Deposit({
    user_id: userData._id,
    amount: amount,
    type: currency,
    users: userData,
});
 
const self={
    payNow:function(paymentData,callback){
        var response ={};

        /* Creating Payment JSON for Paypal starts */
        const payment = {
                "intent": "authorize",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": "http://127.0.0.1:3006/wallet/execute",
                    "cancel_url": "http://127.0.0.1:3006/wallet/cancel"
                },
                "transactions": [{
                    "amount": {
                        "total": paymentData.data.amount,
                        "currency": "USD"
                    },
                    "description": paymentData.data.productName
                }]
        };
        /* Creating Payment JSON for Paypal ends */
     
        /* Creating Paypal Payment for Paypal starts */

        paypal.payment.create(payment, function (error, payment) {
            if (error) {
                console.log(error);
            } else {
                if(payment.payer.payment_method === 'paypal') {
                    response.paymentId = payment.id;
                    var redirectUrl;
                    response.payment = payment;
                    for(var i=0; i < payment.links.length; i++) {
                        var link = payment.links[i];
                        if (link.method === 'REDIRECT') {
                            redirectUrl = link.href;
                        }
                    }
                    // const deposit = createDatabaseDeposit(user, paymentData.data.currency, paymentData.data.amount);
                    //     deposit.save((error, depositToExecute) => { // Update deposit data
                    //         if (error) console.log('2', error) // if error
                    //     });
                    // console.log(deposit);
                    response.redirectUrl = redirectUrl;
                }
            }
            /* 
            * Sending Back Paypal Payment response 
            */
            callback(error,response);
        });
    /* Creating Paypal Payment for Paypal ends */
    },

    getResponse:function(data,PayerID,callback){
        console.log('testing payment');
        var paypalSession = data.session;
        var arr = paypalSession.split(" ");
        console.log(arr);
       // console.log(paypalSession);

        var response = {};
        // const paymentId = data.paypalData.paymentId;
        // const serverAmount = parseFloat(data.paypalData.payment.transactions[0].amount.total);
        // const clientAmount = parseFloat(data.clientData.price);
       
        const details = {
            "payer_id": PayerID 
        };
         
        // response.userData= {
        //     userID : data.sessionData.userID,
        //     name : data.sessionData.name
        // };
         
        // if (serverAmount !== clientAmount) {
        //     response.error = true;
        //     response.message = "Payment amount doesn't matched.";
        //     callback(response);
   //  } else{
            paypal.payment.execute(paymentId, details, function (error, payment) {
                if (error) {
                    console.log(error);
                    response.error = false;
                    response.message = "Payment Successful.";
                    callback(response);
                } else {
                  
                /*
                * inserting paypal Payment in DB
                */
                    // const insertPayment={
                    //     userId : data.sessionData.userID,
                    //     paymentId : paymentId,
                    //     createTime : payment.create_time,
                    //     state : payment.state,
                    //     currency : "USD",
                    //     amount: serverAmount,
                    //     createAt : new Date().toISOString()
                    // }

                /*
                * inserting paypal Payment in Deposit Table
                */
                    const createDatabaseDeposit = (userData, currency, amount) => new Deposit({
                        user_id: userData._id,
                        txid: paymentId,
                        amount: serverAmount,
                        type: "USD",
                        status: 1,
                        users: userData,
                    });
                 
                    self.createDatabaseDeposit(createDatabaseDeposit,function(result){
                        if(! result.isPaymentAdded){
                            response.error = true;
                            response.message = "Payment Successful, but not stored.";
                            callback(response);
                        }else{
                            response.error = false;
                            response.message = "Payment Successful.";
                            callback(response);
                        };
                    });
                };
            });
       // };
    }
}
module.exports = self;