var Setting = require('../models/setting');
var Rate = require('../models/rate');
var Withdraw = require('../models/withdraw');

const Middleware = {

    isAdmin: (req, res, next) => {
        sess = req.session;
        global.user = req.user;

        // Setting data as global
        Setting.findOne({ 'slug': 'setting' }, function(err, settingData) {
            global.setting = settingData;
        });

        // count pending withdrawal
        Withdraw.find({ status: '0' }).sort({ _id: -1 }).count(function(err, withdrawCountData) {
            global.withdrawCount = withdrawCountData;
        });


        // When Ico startDate equal current date
        var nowDate = new Date();
        var currentDt = new Date().toISOString().slice(0, 10);
        Rate.find({}, (err, ratedata) => {
            if (ratedata) {
                ratedata.forEach(function(rates) {
                    if (rates.icoStartDate <= currentDt && rates.icoEndDate >= currentDt) {
                        Setting.findOneAndUpdate({ slug: 'setting' }, {
                            icoStartDate: rates.icoStartDate,
                            icoEndDate: rates.icoEndDate,
                            bonus: rates.bonus,
                        }, function(error, settingData) {

                        });
                    }
                })
            }

        });

        if (sess.is_2fa_otp_validate == 1) {
            res.redirect('/validate');
        } else {
            if (req.user && req.user.role == 0)
                return next();
            else
                res.redirect('/login');
        }
    },

    isUser: (req, res, next) => {
        sess = req.session;
        global.user = req.user;

        // Setting data as global
        Setting.findOne({ 'slug': 'setting' }, function(err, settingData) {
            global.setting = settingData;
        });

        // When Ico startDate equal current date
        var nowDate = new Date();
        var currentDt = new Date().toISOString().slice(0, 10);
        Rate.find({}, (err, ratedata) => {
            if (ratedata) {
                ratedata.forEach(function(rates) {
                    if (rates.icoStartDate <= currentDt && rates.icoEndDate >= currentDt) {
                        Setting.findOneAndUpdate({ slug: 'setting' }, {
                            icoStartDate: rates.icoStartDate,
                            icoEndDate: rates.icoEndDate,
                            bonus: rates.bonus,
                        }, function(error, settingData) {

                        });
                    }
                })
            }

        });

        if (sess.is_2fa_otp_validate == 1) {
            res.redirect('/validate');
        } else {
            if (req.user && req.user.role == 1)
                return next();
            else
                res.redirect('/login');
        }

    },

    isUserAdminAllowed: (req, res, next) => {
        sess = req.session;
        global.user = req.user;
        if (sess.is_2fa_otp_validate == 1) {
            res.redirect('/validate');
        } else {
            if (req.user)
                return next();
            else
                res.redirect('/login');
        }

    }

};


module.exports = Middleware;