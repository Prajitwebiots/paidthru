var express = require('express');
const router = express.Router();
var bCrypt = require('bcryptjs');
var multer = require('multer');
var path = require('path');
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var Email = require('../helpers/emails');
var User = require('../models/user');

// View Profile
router.get('/profile', Middleware.isUserAdminAllowed, (req, res, next) => {
    res.render('profile/profile', { title: 'Edit Profile | PaidThru', 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
});

// Update Profile
router.post('/profile', Middleware.isUserAdminAllowed, (req, res, next) => {
    req.checkBody('firstname', 'First Name is required').notEmpty();
    req.checkBody('lastname', 'Last Name is required').notEmpty();
    const error = req.validationErrors();
    if (error) {
        res.render('profile/profile', { title: 'Edit Profile | PaidThru', 'error': error, 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    } else {

        User.findByIdAndUpdate({ _id: req.user._id }, {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
        }, function(err, user) {
            if (err)
                console.log(err);
            else
                req.flash('successMessage', 'Profile updated successfully.');
            res.redirect('/profile');
        });
    }
});

// Update profile picture
router.post('/updateProfile', Middleware.isUserAdminAllowed, (req, res, next) => {
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 1000000
        },
        fileFilter: function(req, file, callback) {
            var ext = path.extname(file.originalname)
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                return callback(req.flash('errorMessage', 'Only images are allowed'), null)
            }
            callback(null, true)
        }
    }).single('imageUpload');
    upload(req, res, function(err) {
        if (err) {
            req.flash('errorMessage', "!" + err);
            res.redirect('/profile');
        } else {
            if (req.file != undefined) {
                User.findByIdAndUpdate({ _id: req.user._id }, {
                    profile: req.file.filename
                }, function(err, docs) {
                    if (err)
                        console.log(err)
                    else
                        req.flash('successMessage', 'Profile picture is updated.');
                    res.redirect('/profile');

                });

            } else if (req.file == undefined) {
                req.flash('errorMessage', "Please choose profile picture.");
                res.redirect('/profile');
            }
        }
    })
});


// View changePassword
router.get('/changePassword', Middleware.isUserAdminAllowed, (req, res, next) => {
    res.render('profile/changepassword', { title: 'Change Password | PaidThru', 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
});


// Update password
router.post('/changePassword', Middleware.isUserAdminAllowed, (req, res, next) => {

    req.checkBody('currentpassword', 'Current Password is required').notEmpty();
    req.checkBody('new_password', 'New Password is required').notEmpty();
    req.checkBody('confirm_password', 'Confirm Password is required').notEmpty();

    const error = req.validationErrors();
    if (error) {
        res.render('profile/changepassword', { title: 'Change Password | PaidThru', 'error': error, 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    } else {
        var password = req.body.currentpassword;
        if (!isValidPassword(req.user, password)) {
            req.flash('errorMessage', 'Invalid Current Password');
            res.redirect('/changePassword');
        } else {
            if (req.body.new_password !== req.body.confirm_password) {
                req.flash('errorMessage', 'password and confirm password do not match.');
                res.redirect('/changePassword');
            } else {
                var user = req.user;
                user.password = Helper.bcrypt(req.body.new_password);
                user.save(function(err) {
                    if (err) { next(err) } else {
                        req.flash('successMessage', 'Password updated successfully.');
                        res.redirect('/changePassword');
                    }
                })
            }
        }
    }

});


// Update KYC
router.post('/uploadKyc', Middleware.isUserAdminAllowed, (req, res, next) => {

    var upload = multer({
        storage: storage_kyc,
        limits: {
            fileSize: 5000000
        },
        fileFilter: function(req, file, callback) {
            var ext = path.extname(file.originalname)
            if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                return callback(req.flash('errorMessage', 'Only images are allowed'), null)
            }
            callback(null, true)
        }
    }).any();
    upload(req, res, function(err) {
        if (err) {
            req.flash('errorMessage', "Opps!" + err);
            res.redirect('/profile');
        } else {
            var kyc_files = [];
            for (var i = 0; i < req.files.length; i++) {
                if (req.files[i]['fieldname'] == 'first_id_proof') { kyc_files['front'] = req.files[i]['filename']; } else if (req.files[i]['fieldname'] == 'second_id_proof') { kyc_files['backend'] = req.files[i]['filename']; } else if (req.files[i]['fieldname'] == 'third_id_proof') { kyc_files['selfie'] = req.files[i]['filename']; }
            }
            if (kyc_files['front'] && kyc_files['backend'] && kyc_files['selfie']) {
                User.findByIdAndUpdate({ _id: req.user._id }, {
                    kyc_front: kyc_files['front'],
                    kyc_back: kyc_files['backend'],
                    kyc_selfie: kyc_files['selfie']
                }, function(err, docs) {
                    if (err) res.send(err);
                    else {
                        Email.sendKycForAdminEmail(req.user);
                        req.flash('successMessage', 'KYC is uploaded Successfully.');
                        res.redirect('/profile');
                    }
                });
            } else {
                req.flash('errorMessage', "KYC fields are required.");
                res.redirect('/profile');
            }

        }
    })


});



function isValidPassword(user, password) {
    return bCrypt.compareSync(password, user.password);
}


var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/users')
    },
    filename: function(req, file, callback) {
        callback(null, req.user.username + '-' + Date.now() + path.extname(file.originalname))
    }
})

var storage_kyc = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/kyc')
    },
    filename: function(req, file, callback) {
        callback(null, req.user._id + '-' + Date.now() + path.extname(file.originalname))
    }
})


module.exports = router;