var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var BuyToken = require('../models/buytoken');
var Rate = require('../models/rate');
var Setting = require('../models/setting');


// create Deposit
const createDatabaseBuyToken = (userData, coin, totalToken, tokenPrice, txid) => new BuyToken({
    user_id: userData._id,
    txid: txid,
    amount: tokenPrice,
    type: coin,
    tokens: totalToken,
    users: userData,
});


// View ICO Information
router.get('/ico-information', Middleware.isUser, (req, res, next) => {
    Rate.find({}).exec(function(err, rateData) {
        res.render('user/ico-information', { title: 'Ico Information | PaidThru', rate: rateData, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// View Buy Token (ICO)
router.get('/buy-token', Middleware.isUser, (req, res, next) => {
    BuyToken.find({ user_id: user._id }).sort({ _id: -1 }).exec(function(err, buytoken) {
        res.render('user/buy-token', { title: 'Buy Token | PaidThru', buytoken: buytoken, setting: setting, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// Buy Token (ICO)
router.post('/buy-token', Middleware.isUser, (req, res, next) => {

    if (!req.body.coin) {
        req.flash('errorMessage', 'Please select coin type.');
        res.redirect('/buy-token');
    } else if (!req.body.numberOfToken) {
        req.flash('errorMessage', 'Please enter number of token.');
        res.redirect('/buy-token');
    }

    var coin = req.body.coin;
    var numberOfToken = req.body.numberOfToken;

    var btc_bal = parseFloat(user.btc_bal);
    var eth_bal = parseFloat(user.eth_bal);
    var ltc_bal = parseFloat(user.ltc_bal);
    var doge_bal = parseFloat(user.doge_bal);
    var usd_bal = parseFloat(user.usd_bal);
    var eur_bal = parseFloat(user.eur_bal);
    var gbp_bal = parseFloat(user.gbp_bal);
    var cad_bal = parseFloat(user.cad_bal);
    var rate = parseFloat(setting.rate);
    var soldToken = setting.soldToken;
    var tokenBonus = setting.bonus;
    var refBonus = setting.ref_bonus;

    if (numberOfToken > 0) {

        if (coin == 'btc') {
            var coinName = 'btc_bal';
            var balance = btc_bal.toFixed(8); // user btc bal
            var price = parseFloat(setting.btc_price); // btc price
            var tokens = parseFloat(numberOfToken) * rate; //  1000 * 0.099 ex.
            var tokenPrice = (tokens / price).toFixed(8); // token btc amt
        } else if (coin == 'eth') {
            var coinName = 'eth_bal';
            var balance = eth_bal.toFixed(8);
            var price = parseFloat(setting.eth_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(8);
        } else if (coin == 'ltc') {
            var coinName = 'ltc_bal';
            var balance = ltc_bal.toFixed(8);
            var price = parseFloat(setting.ltc_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(8);
        } else if (coin == 'doge') {
            var coinName = 'doge_bal';
            var balance = doge_bal.toFixed(8);
            var price = parseFloat(setting.doge_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(8);
        } else if (coin == 'usd') {
            var coinName = 'usd_bal';
            var balance = usd_bal.toFixed(2);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = tokens;
        } else if (coin == 'eur') {
            var coinName = 'eur_bal';
            var balance = eur_bal.toFixed(2);
            var price = parseFloat(setting.eur_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(2);
        } else if (coin == 'gbp') {
            var coinName = 'gbp_bal';
            var balance = gbp_bal.toFixed(2);
            var price = parseFloat(setting.gbp_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(2);
        } else if (coin == 'cad') {
            var coinName = 'cad_bal';
            var balance = cad_bal.toFixed(2);
            var price = parseFloat(setting.cad_price);
            var tokens = parseFloat(numberOfToken) * rate;
            var tokenPrice = (tokens / price).toFixed(2);
        }



        if (parseFloat(tokenPrice) > parseFloat(balance)) {

            req.flash('errorMessage', 'Insufficient balance.');
            res.redirect('/buy-token');

        } else {

            var bonus = parseFloat(numberOfToken) * tokenBonus / 100;
            var referralBonus = parseFloat(numberOfToken) * refBonus / 100;
            var total = parseFloat(bonus) + parseFloat(numberOfToken);
            var totalBonus = parseFloat(referralBonus) + parseFloat(total);
            var deductBal = parseFloat(balance) - parseFloat(tokenPrice);
            var addToken = parseFloat(user.token_bal) + parseFloat(total);
            var txid = 'PA' + Helper.randomString(20) + 'IDEX';
            const buytoken = createDatabaseBuyToken(user, coin, total, tokenPrice, txid); // pass data
            var soldedToken = parseFloat(soldToken) + parseFloat(total);
            var soldedTokenBonus = parseFloat(soldToken) + parseFloat(totalBonus);


            if (!user.sponser) {

                buytoken.save((error, buyTokenExe) => { // Update buyTokenExe data
                    Setting.findOneAndUpdate({ slug: 'setting' }, { soldToken: soldedToken }, function(error, settingData) {
                        User.findByIdAndUpdate({ _id: req.user._id }, {
                            [coinName]: deductBal,
                            "token_bal": addToken
                        }, (error, executedToken) => {
                            req.flash('successMessage', 'You have purchased ' + total + ' token.');
                            res.redirect('/buy-token');
                        });
                    });
                });

            } else {
                buytoken.save((error, buyTokenExe) => { // Update buyTokenExe data
                    Setting.findOneAndUpdate({ slug: 'setting' }, { soldToken: soldedTokenBonus }, function(error, settingData) {
                        User.findByIdAndUpdate({ _id: req.user._id }, {
                            [coinName]: deductBal,
                            "token_bal": addToken
                        }, (error, executedToken) => {
                            User.findOne({ username: user.sponser }, (error, usersBal) => {
                                var refb = parseFloat(usersBal.token_bal) + parseFloat(referralBonus);
                                User.findOneAndUpdate({ username: user.sponser }, { "token_bal": refb }, (error, users) => {
                                    if (error)
                                        console.log('1', error)
                                    else
                                        req.flash('successMessage', 'You have purchased ' + total + ' token.');
                                    res.redirect('/buy-token');
                                });
                            });
                        });
                    });
                });

            }

        }

    }

});


module.exports = router;