var express = require('express');
const router = express.Router();
var request = require('request');
var cron = require('node-cron');
var Setting = require('../models/setting');
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');


/********** Admin Side **********/

// create Setting
const createDatabaseSetting = () => new Setting({
    icoStartDate: '11-8-2018',
    icoEndDate: '11-9-2018',
    adminEmail: 'admin@gmail.com',
    totalSupplyToken: '1000000000',
    soldToken: '0',
    rate: '0.2',
    bonus: '15',
    ref_bonus: '15',
    btc_price: '6130.34',
    eth_price: '317.79',
    ltc_price: '56.26',
    withdrawalStatus: '0',
    slug: 'setting',
});


//  create setting database
router.get('/createDatabase', Middleware.isAdmin, (req, res, next) => {
    const setting = createDatabaseSetting();
    setting.save((error, setting) => {
        if (error) {
            console.log('1', error);
        } else {
            console.log('Setting successfully created..')
        }
    })
});


// Cron job for btc-eth-ltc-eur-gbp price update every min
cron.schedule('*/1 * * * *', function() {

    // BTC - USD
    request('https://www.bitstamp.net/api/v2/ticker/btcusd', (error, response, body) => {

        const data = JSON.parse(body);
        var btc_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { btc_price: btc_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // ETH - USD
    request('https://www.bitstamp.net/api/v2/ticker/ethusd', (error, response, body) => {

        const data = JSON.parse(body);
        var eth_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { eth_price: eth_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // LTC - USD
    request('https://www.bitstamp.net/api/v2/ticker/ltcusd', (error, response, body) => {

        const data = JSON.parse(body);
        var ltc_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { ltc_price: ltc_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // DOGE - USD
    request('https://min-api.cryptocompare.com/data/price?fsym=DOGE&tsyms=USD', (error, response, body) => {

        const data = JSON.parse(body);
        var doge_price = (data.USD);
        var myquery = { slug: "setting" };
        var newvalues = { doge_price: doge_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // EURO - USD
    request('https://www.bitstamp.net/api/v2/ticker/eurusd', (error, response, body) => {

        const data = JSON.parse(body);
        var eur_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { eur_price: eur_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // GBP - USD
    request('https://min-api.cryptocompare.com/data/price?fsym=GBP&tsyms=USD', (error, response, body) => {

        const data = JSON.parse(body);
        var gbp_price = (data.USD);
        var myquery = { slug: "setting" };
        var newvalues = { gbp_price: gbp_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

    // CAD - USD
    request('https://min-api.cryptocompare.com/data/price?fsym=CAD&tsyms=USD', (error, response, body) => {

        const data = JSON.parse(body);
        var cad_price = (data.USD);
        var myquery = { slug: "setting" };
        var newvalues = { cad_price: cad_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {

        });
    });

})


// Show Settings
router.get('/', Middleware.isAdmin, (req, res, next) => {
    Setting.findOne({ 'slug': 'setting' }, function(err, settingData) {
        res.render('admin/setting', { title: 'Setting | PaidThru', setting: settingData, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});


// Update Settings
router.post('/update', Middleware.isAdmin, (req, res, next) => {
    Setting.findOneAndUpdate({ slug: 'setting' }, {
        totalSupplyToken: req.body.totalSupplyToken,
        rate: req.body.rate,
        ref_bonus: req.body.ref_bonus,
        game: req.body.game,
        withdrawalStatus: req.body.withdrawalStatus,
    }, function(error, settingData) {
        if (error) {
            console.log(error)
        } else {
            req.flash('success', ' Setting Update Successfully.');
            res.redirect('/setting');
        }
    });
})


module.exports = router;