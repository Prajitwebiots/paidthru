var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var User = require('../models/user');
var Login = require('../models/login');
var Withdraw = require('../models/withdraw');
var Deposit = require('../models/deposit');
var Rate = require('../models/rate');
var Setting = require('../models/setting');


router.get('/dashboard', Middleware.isUserAdminAllowed, (req, res, next) => {

    startDate = new Date();
    startDate.setHours('05', '30', '00', '400'); // todays start hr
    endDate = new Date();
    endDate.setHours('29', '29', '00', '400'); // todays end  hr

    // When Ico startDate equal current date
    var nowDate = new Date();
    var currentDt = new Date().toISOString().slice(0, 10);
    Rate.find({}, (err, ratedata) => {
        if (ratedata) {
            ratedata.forEach(function(rates) {
                if (rates.icoStartDate <= currentDt && rates.icoEndDate >= currentDt) {
                    Setting.findOneAndUpdate({ slug: 'setting' }, {
                        icoStartDate: rates.icoStartDate,
                        icoEndDate: rates.icoEndDate,
                        bonus: rates.bonus,
                    }, function(error, settingData) {

                    });
                }
            })
        }

    });

    // count pending withdrawal
    Withdraw.find({ status: '0' }).sort({ _id: -1 }).count(function(err, withdrawCountData) {
        global.withdrawCount = withdrawCountData;
    });

    // Setting data as global
    Setting.findOne({ 'slug': 'setting' }, function(err, settingData) {
        global.setting = settingData;
    });

    // Todays Login | withdraw | deposit history showed on dashboard 
    Login.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, logindata) {
        Withdraw.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, withdrawdata) {
            Deposit.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, depositdata) {
                Rate.find({}).exec(function(err, rateData) {
                    res.render('dashboard/dashboard', { title: 'Dashboard | PaidThru', rate: rateData, setting: setting, deposit: depositdata, withdraw: withdrawdata, login: logindata });
                });
            });
        });
    });

});


module.exports = router;