var express = require('express');
const router = express.Router();
var speakeasy = require('speakeasy');
var Middleware = require('../helpers/middleware');
var Email = require('../helpers/emails');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var Withdraw = require('../models/withdraw');
var Coinpayments = require('coinpayments');
var options = require('../config/coinpaymentsConfig.js');
var client = new Coinpayments(options);

/********** User Side **********/


// create Withdraw
const createDatabaseWithdraw = (body, user) => new Withdraw({
    user_id: user._id,
    amount: body.amount,
    address: body.address,
    type: body.coin,
    users: user,
});


// View withdrawal 
router.get('/withdraw/:coin', Middleware.isUser, (req, res, next) => {
    var coin = req.params.coin; // coin btc,ltc eth
    Withdraw.find({ user_id: user._id, type: coin }).sort({ _id: -1 }).exec(function(err, withdraw) {
        res.render('user/withdraw', { title: 'withdraw | PaidThru', withdraw: withdraw, coin: coin, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});



// Proceed to withdrwal
router.post('/withdraw', Middleware.isUser, (req, res, next) => {

    var coin = req.body.coin; // coin btc ltc etc
    var address = req.body.address; // address
    var amount = req.body.amount; // amount
    var otp = req.body.google_2fa_otp; // otp

    if (coin == "btc") {
        var userBal = req.user.btc_bal; // user btc bal
        var coinName = 'btc_bal';
    } else if (coin == "eth") {
        var userBal = req.user.eth_bal; // user eth bal
        var coinName = 'eth_bal';
    } else if (coin == "ltc") {
        var userBal = req.user.ltc_bal; // user ltc bal
        var coinName = 'ltc_bal';
    } else if (coin == "paid") {
        var userBal = req.user.token_bal; // user token bal
        var coinName = 'token_bal';
    }
    if (setting.withdrawalStatus == null) { // withdrwal off
        req.flash('errorMessage', 'Withdrwal is disabled.');
        res.redirect('/wallet/withdraw/' + coin);
    } else if (address == '') { // if address blank
        req.flash('errorMessage', 'Address fields is require');
        res.redirect('/wallet/withdraw/' + coin);
    } else if (amount == '') { // if amount blank
        req.flash('errorMessage', 'Amount fields is require');
        res.redirect('/wallet/withdraw/' + coin);
    } else if (req.user.isset_2fa == '0') { // if 2fa off
        req.flash('errorMessage', 'Please turn on Google 2FA for security purpose.');
        res.redirect('/wallet/withdraw/' + coin);
    } else if (req.user.kyc_status == '0' || req.user.kyc_status == '-1') { // if kyc 
        req.flash('errorMessage', 'Please update your KYC document.');
        res.redirect('/wallet/withdraw/' + coin);
    } else if (userBal >= amount) {

        var base32secret = req.user.g_2fa_base32; // check 2fa otp
        var verified = speakeasy.totp.verify({
            secret: base32secret,
            encoding: 'base32',
            token: otp
        });

        if (verified == 1) { // if otp verified 
            const withdraw = createDatabaseWithdraw(req.body, req.user); // create withdrawal
            withdraw.save((error, withdraw) => {
                var finalAmount = parseFloat(userBal) - parseFloat(amount); // final amont
                User.findByIdAndUpdate({ _id: req.user._id }, { // update final bal
                    [coinName]: finalAmount
                }, (error, users) => {

                    if (error) {
                        req.flash('errorMessage', 'Error' + error) // error
                        res.redirect('/wallet/withdraw/' + coin);
                    } else {
                        req.flash('successMessage', 'Your withdrawal request has been sended.') // success
                        res.redirect('/wallet/withdraw/' + coin);
                    }

                });
            });
        } else {
            req.flash('errorMessage', 'OTP Does not match, Please try again.') // otp not match
            res.redirect('/wallet/withdraw/' + coin);
        }

    } else {
        req.flash('errorMessage', 'Insufficient User Balance') // insuffiecient bal
        res.redirect('/wallet/withdraw/' + coin);
    }


});




/********** Admin Side **********/

// Reject withdrawal request from admin
router.post('/withdraw/reject', Middleware.isAdmin, (req, res, next) => {

    var coin = req.body.coin; // coin btc ltc etc
    var amount = req.body.amount; // amount
    var user_id = req.body.user_id; // uid

    User.findOne({ '_id': user_id }, function(err, user) {
        if (user) {
            if (coin == "btc") {
                var userBal = user.btc_bal; // user btc bal
                var coinName = 'btc_bal';
            } else if (coin == "eth") {
                var userBal = user.eth_bal; // user eth bal
                var coinName = 'eth_bal';
            } else if (coin == "ltc") {
                var userBal = user.ltc_bal; // user ltc bal
                var coinName = 'ltc_bal';
            } else if (coin == "doge") {
                var userBal = user.doge_bal; // user ltc bal
                var coinName = 'doge_bal';
            } else if (coin == "paid") {
                var userBal = user.token_bal; // user token bal
                var coinName = 'token_bal';
            }

            var finalAmount = parseFloat(userBal) + parseFloat(amount); // final amont

            User.findByIdAndUpdate({ _id: user._id }, { // update final bal
                [coinName]: finalAmount
            }, (error, usersExc) => {
                if (error) {
                    console.log(error)
                } else {
                    Email.sendRejectWithdrawalEmail(user, coin, amount);
                    Withdraw.findByIdAndUpdate({ _id: req.body.id }, {
                        status: '4'
                    }, (error, withdrawReject) => {
                        res.redirect('/transaction/withdraws');
                    });
                }
            });

        } else {
            req.flash('errorMessage', 'Something Went Wrong') // insuffiecient bal
            res.redirect('/transaction/withdraws');
        }
    });

});



// Approve withdrawal request from admin coinpayment
router.post('/withdraw/approve', Middleware.isAdmin, (req, res, next) => {
    Withdraw.findOne({ _id: req.body.id }, function(error, withdraw) {
        if (withdraw.type == 'paid') {
            Withdraw.findOne({ _id: req.body.id }, function(error, withdraw) {
                Withdraw.findByIdAndUpdate({ _id: withdraw._id }, { status: 1 }, function(error, excWithdrawal) {
                    if (error) {
                        req.flash('errorMessage', error);
                        res.redirect('/transaction/withdraws');
                    } else {
                        res.redirect('/transaction/withdraws');
                    }
                });
            });
        } else {
            if (withdraw.type == 'btc') { var currency = 'BTC' } else if (withdraw.type == 'eth') { var currency = 'ETH' } else if (withdraw.type == 'ltc') { var currency = 'LTC' } else if (withdraw.type == 'doge') { var currency = 'DOGE' }
            client.createWithdrawal({ 'currency': currency, 'amount': withdraw.amount, 'address': withdraw.address }, function(error, result) {
                if (error) {
                    req.flash('errorMessage', error);
                    res.redirect('/transaction/withdraws');
                } else {
                    Withdraw.findByIdAndUpdate({ _id: withdraw._id }, { withdrawalId: result.id, status: -1 }, function(error, excWithdrawal) {
                        if (error) {
                            req.flash('errorMessage', error);
                            res.redirect('/transaction/withdraws');
                        } else {
                            res.redirect('/transaction/withdraws');
                        }
                    });
                }
            });
        }
    });
});

module.exports = router;