var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var User = require('../models/user');

// View wallet 
router.get('/', Middleware.isUser, (req, res, next) => {
    res.render('user/wallet', { title: 'Wallet | PaidThru', setting: setting, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
});

module.exports = router;