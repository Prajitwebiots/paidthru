var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var Deposit = require('../models/deposit');
var CoinAddress = require('../models/coinaddress');
var Coinpayments = require('coinpayments');
var options = require('../config/coinpaymentsConfig.js');
var QRCode = require('qrcode');
var client = new Coinpayments(options);



/********** Cryptocurrency **********/

// View Deposit and get callback address from coinpayments
router.get('/deposit/:coin', Middleware.isUser, (req, res, next) => {

    var coin = req.params.coin;
    if(coin == 'btc'){ var cn = 'BTC' } else if(coin == 'eth'){ var cn = 'ETH'} else if(coin == 'ltc'){ var cn = 'LTC' } else if(coin == 'doge'){ var cn = 'DOGE' }
        
    Deposit.find({ user_id: user._id, type: cn }).sort({ _id: -1 }).exec(function(err, deposit) { // start deposit decending order
        CoinAddress.findOne({ 'user_id': user._id, 'type': coin }, function(err, data) { // start coinaddress
            if (!data || data.length == 0) { // start loop
                client.getCallbackAddress(cn, function(err, response) { // call api coinpayment
                    if (response.address) {
                        CoinAddress({ user_id: user._id, coin: coin, address: response.address, type: coin, users: req.user }).save(function(err, coinaddress) {
                            QRCode.toDataURL(coinaddress.address, function(err, QRCode) {
                                res.render('user/deposit', { title: 'Deposit | PaidThru', deposit: deposit, QRCode: QRCode, address: coinaddress.address, coin: coin, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                            });
                        })
                    }
                })
            } else {
                QRCode.toDataURL(data.address, function(err, QRCode) { // qrcode images
                    res.render('user/deposit', { title: 'Deposit | PaidThru',   deposit: deposit, QRCode: QRCode, address: data.address, coin: coin, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                });
            } // end loop
        }); // end coinaddress
    }); // end deposit

});


/********** FIAT Currency **********/


module.exports = router;