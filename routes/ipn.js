var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var Email = require('../helpers/emails');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var Deposit = require('../models/deposit');
var Withdraw = require('../models/withdraw');
var CoinAddress = require('../models/coinaddress');
var Coinpayments = require('coinpayments');
var options = require('../config/coinpaymentsConfig.js');

// create Deposit
const createDatabaseDeposit = (userData, ipnData) => new Deposit({
    user_id: userData._id,
    amount: ipnData.amount,
    address: ipnData.address,
    txid: ipnData.txn_id,
    type: ipnData.currency,
    users: userData,
});


// Coinpayment IPN Call-back
const events = Coinpayments.events;

const middlewareIpn = [
    Coinpayments.ipn({
        merchantId: options.merchantId,
        merchantSecret: options.merchantSecret,
    }),
    (req, res, next) => {}
];

router.use('/ipn-callback', middlewareIpn);


/***********  Start IPN Completed*************/

events.on('ipn_complete', (ipnData) => { // get ipn from coinpayment

    if (ipnData.ipn_type == "withdrawal") { // is Withdrawal ipn 

        Withdraw.findOne({ withdrawalId: ipnData.id }, function(error, withdraw) { // withdrwal find by txid
            if (error)
                console.log('1', error); // if error
            else
                Withdraw.findByIdAndUpdate({ _id: withdraw._id }, { txid: ipnData.txn_id, status: 1 }, function(error, withdrawExecuted) { // update status
                    if (error) // if error
                        console.log('2', error);
                    else
                        console.log("withdraw success"); // if success
                });

        }); // end withdrawal find

    } else { // else is deposit ipn

        if (ipnData.status >= 100) { // 100 status is payment completed

            CoinAddress.findOne({ address: ipnData.address }, (error, addressData) => { // find address in coinAddress table

                User.findOne({ _id: addressData.user_id }, (error, userData) => { // get user data

                    if (ipnData.currency == 'BTC') {
                        var userAmt = userData.btc_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "btc_bal";
                    } else if (ipnData.currency == 'ETH') {
                        var userAmt = userData.eth_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "eth_bal";
                    } else if (ipnData.currency == 'LTC') {
                        var userAmt = userData.ltc_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "ltc_bal";
                    } else if (ipnData.currency == 'DOGE') {
                        var userAmt = userData.doge_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "doge_bal";
                    }

                    var finalAmount = parseFloat(userAmt) + parseFloat(amount); // finalamount 

                    Deposit.findOne({ txid: ipnData.txn_id }, (error, depositData) => { // check in deposit if status 0 and txid 

                        if (depositData) { // if depositData

                            Deposit.findOneAndUpdate({ _id: depositData._id, status: 0 }, { status: 100 }, (error, depositToExecute) => {
                                if (depositToExecute) {
                                    User.findByIdAndUpdate({ _id: userData._id }, {
                                        [coin_bal_name]: finalAmount
                                    }, (error, invoiceToExecute) => {
                                        if (error)
                                            console.log('1', error)
                                        else
                                            Email.sendDepositEmail(userData, ipnData.currency, ipnData.amount);
                                    });
                                }
                            });

                        } else { // else depositData

                            const deposit = createDatabaseDeposit(userData, ipnData); // Update deposit data
                            deposit.save((error, depositToExecute) => {
                                if (error)
                                    console.log('2', error) // if error
                                else
                                    Deposit.findOneAndUpdate({ _id: depositToExecute._id, status: 0 }, { status: 100 }, (error, dpstToExecute) => {
                                        User.findByIdAndUpdate({ _id: userData._id }, {
                                            [coin_bal_name]: finalAmount
                                        }, (error, invoiceToExecute) => {
                                            if (error)
                                                console.log('2', error)
                                            else
                                                Email.sendDepositEmail(userData, ipnData.currency, ipnData.amount);
                                        });
                                    });
                            }); // End deposit saving

                        } // End depositData if loop 


                    }); // End depositData

                }); // End userData

            }); // End addressData (coinAddress)

        } // End If loop

    } // end if else loop

}); // End Event ipn_completed


/***********  End IPN Completed*************/



/***********  Start IPN Failed *************/

events.on('ipn_fail', (ipnData) => { // fail ipn

    if (ipnData.ipn_type == "withdrawal") { // is Withdrawal ipn 

        Withdraw.findOne({ withdrawalId: ipnData.id }, function(error, withdraw) { // start find withdrwal txid

            if (withdraw) { // if withdrawal data

                User.findOne({ _id: withdraw.user_id }, (error, userData) => { // get user data

                    if (ipnData.currency == 'BTC') { // currency balance
                        var userAmt = userData.btc_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "btc_bal";
                    } else if (ipnData.currency == 'ETH') {
                        var userAmt = userData.eth_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "eth_bal";
                    } else if (ipnData.currency == 'LTC') {
                        var userAmt = userData.ltc_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "ltc_bal";
                    } else if (ipnData.currency == 'DOGE') {
                        var userAmt = userData.doge_bal;
                        var amount = ipnData.amount;
                        var coin_bal_name = "doge_bal";
                    }

                    var finalAmount = parseFloat(userAmt) + parseFloat(amount); // finalamount 

                    User.findByIdAndUpdate({ _id: withdraw.user_id }, { // start user update
                        [coin_bal_name]: finalAmount // Update user balance
                    }, (error, invoiceToExecute) => {

                        if (error) {
                            console.log('1', error) // if error
                        } else {
                            Withdraw.findByIdAndUpdate({ _id: withdraw._id }, { txid: ipnData.txn_id, status: 3 }, function(error, result) {
                                if (result) {
                                    console.log("withdraw fail from coin payment");
                                }
                            })

                        } // end loop

                    }); // end user

                }); // end userdata

            } // end if loop

        }); // end withdrawal data

    } else { // else is deposit ipn

        if (ipnData.status == -1) { // -1 status is payment failed

            CoinAddress.findOne({ address: ipnData.address }, (error, addressData) => { // find address in coinAddress table

                if (addressData) {

                    User.findOne({ _id: addressData.user_id }, (error, userData) => { // get user data

                        Deposit.findOne({ txid: ipnData.txn_id }, (error, depositData) => { // check in deposit if status 0 and txid 

                            if (depositData) { // if depositData

                                Deposit.findOneAndUpdate({ _id: depositData._id, status: 0 }, { status: -1 }, (error, depositToExecute) => {
                                    if (error)
                                        console.log('1', error)
                                    else
                                        console.log(depositToExecute)
                                });

                            } else { // else depositData

                                const deposit = createDatabaseDeposit(userData, ipnData); // Update deposit data
                                deposit.save((error, depositToExecute) => {
                                    if (error) {
                                        console.log('3', error) // if error
                                    } else {
                                        Deposit.findOneAndUpdate({ _id: depositData._id, status: 0 }, { status: -1 }, (error, depositToExecute) => {

                                        });
                                    }

                                }); // End deposit saving

                            } // End depositData if loop

                        }); // End depositData
                    });
                }

            }); // End addressData (coinAddress)

        } // End If loop

    } // end if else loop

});

/***********  End IPN Failed*************/

module.exports = router;