var express = require('express');
const router = express.Router();
var cron = require('node-cron');
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var BuyToken = require('../models/buytoken');
var Rate = require('../models/rate');
var Setting = require('../models/setting');
var Game = require('../models/game');
var GameHistory = require('../models/gamehistory');
var DailyWinner = require('../models/dailywinner');
var WeeklyWinner = require('../models/weeklywinner');
var MonthlyWinner = require('../models/monthlywinner');
var JackpotWinner = require('../models/jackpotwinner');

// create Setting
const createDatabaseGame = () => new Game({
    name: 'Jackpot',
    token: '50',
});

// create GameHistory
const createDatabaseGameHistory = (computerNumber, userNumber, token, user) => new GameHistory({
    user_id: user._id,
    user_number: userNumber,
    computer_number: computerNumber,
    score: Math.abs(userNumber - computerNumber),
    token: token,
    users: user,
});

// create WinDaily
const createDatabaseWinDaily = (dailyData, user_id, user_number, computer_number, score, user) => new DailyWinner({
    user_id: user_id,
    name: 'daily',
    user_number: user_number,
    computer_number: computer_number,
    score: score,
    tokens: dailyData.tokens,
    note: '1st winner',
    users: user,
});

// create WinWeekly
const createDatabaseWinWeekly = (tokenPrize, user_id, user_number, computer_number, score, user, winner) => new WeeklyWinner({
    user_id: user_id,
    name: 'weekly',
    user_number: user_number,
    computer_number: computer_number,
    score: score,
    tokens: tokenPrize,
    note: winner,
    users: user,
});

// create WinMonthly
const createDatabaseWinMonthly = (tokenPrize, user_id, user_number, computer_number, score, user, winner) => new MonthlyWinner({
    user_id: user_id,
    name: 'monthly',
    user_number: user_number,
    computer_number: computer_number,
    score: score,
    tokens: tokenPrize,
    note: winner,
    users: user,
});

// create WinJackpot
const createDatabaseWinJackpot = (tokenPrize, user_id, user_number, computer_number, score, user) => new JackpotWinner({
    user_id: user_id,
    name: 'jackpot',
    user_number: user_number,
    computer_number: computer_number,
    score: score,
    tokens: tokenPrize,
    note: 'Jackpot Winner',
    users: user,
});


/********** Admin Side **********/

//  create game database
router.get('/createDatabaseGame', Middleware.isAdmin, (req, res, next) => {
    const game = createDatabaseGame();
    game.save((error, setting) => {
        if (error) {
            console.log('1', error);
        } else {
            console.log('Game successfully created..')
        }
    })
});

// view transaction deposit
router.get('/game/histories', Middleware.isAdmin, (req, res, next) => {
    Game.find({}).exec(function(err, gameData) {
        DailyWinner.find({}).populate('users').sort({ score: 0 }).exec(function(err, dailyWinner) {
            WeeklyWinner.find({}).populate('users').sort({ score: 0 }).exec(function(err, weeklyWinner) {
                MonthlyWinner.find({}).populate('users').sort({ score: 0 }).exec(function(err, monthlyWinner) {
                    JackpotWinner.find({}).populate('users').sort({ score: 0 }).exec(function(err, jackpotWinner) {
                        res.render('admin/gamehistory', { title: 'Game History | PaidThru', game: gameData, dailyWinner: dailyWinner, weeklyWinner: weeklyWinner, monthlyWinner: monthlyWinner, jackpotWinner: jackpotWinner, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                    });
                });
            });
        });
    });
});


// View and Edit Rate
router.get('/game/setting', Middleware.isAdmin, (req, res, next) => {
    Game.find({}).exec(function(err, gameData) {
        res.render('admin/game', { title: 'Game Setting | PaidThru', game: gameData });
    });
});


// Update game setting
router.post('/game/update', Middleware.isAdmin, (req, res, next) => {
    Game.findByIdAndUpdate({ _id: req.body.game_id }, {
        name: req.body.name,
        percentage: req.body.percentage,
    }, function(error, gameData) {
        if (error) {
            console.log(error)
        } else {
            req.flash('success', ' Rate Update Successfully.');
            res.redirect('/game/setting');
        }
    });
});

/********** User Side **********/

// View Game
router.get('/game', Middleware.isUser, (req, res, next) => {
    Game.find({}).exec(function(err, gameData) {
        GameHistory.findOne({ user_id: req.user._id }).sort({ _id: -1 }).exec(function(error, gameHisData) {
            res.render('user/game', { title: 'Game | PaidThru', user: req.user, game: gameData, gameHistory: gameHisData, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
        });
    });
});

// play game
router.post('/game', Middleware.isUser, (req, res, next) => {

    var computerNumber = Math.floor(100000 + Math.random() * 900000);
    var userNumber = req.body.numberChoose;
    var token = setting.game;

    if (req.user.token_bal >= token) {

        const gameHistory = createDatabaseGameHistory(computerNumber, userNumber, token, req.user);
        gameHistory.save((error, game) => {
            if (error) {
                console.log('1', error);
            } else {

                Game.findOne({ slug: 'company' }, function(error, companyData) {
                    var compToken = parseFloat(token) * companyData.percentage / 100;
                    var companyToken = parseFloat(companyData.tokens + compToken);
                    Game.findOneAndUpdate({ slug: 'company' }, { tokens: companyToken }, function(error, company) {});
                });

                Game.findOne({ slug: 'daily' }, function(error, dailyData) {
                    var dToken = parseFloat(token) * dailyData.percentage / 100;
                    var dailyToken = parseFloat(dailyData.tokens + dToken);
                    Game.findOneAndUpdate({ slug: 'daily' }, { tokens: dailyToken }, function(error, daily) {});
                });


                Game.findOne({ slug: 'weekly' }, function(error, weeklyData) {
                    var wToken = parseFloat(token) * weeklyData.percentage / 100;
                    var weeklyToken = parseFloat(weeklyData.tokens + wToken);
                    Game.findOneAndUpdate({ slug: 'weekly' }, { tokens: weeklyToken }, function(error, weekly) {});
                });

                Game.findOne({ slug: 'monthly' }, function(error, monthlyData) {
                    var wToken = parseFloat(token) * monthlyData.percentage / 100;
                    var monthlyToken = parseFloat(monthlyData.tokens + wToken);
                    Game.findOneAndUpdate({ slug: 'monthly' }, { tokens: monthlyToken }, function(error, monthly) {});
                });

                Game.findOne({ slug: 'jackpot' }, function(error, jackpotData) {
                    var jToken = parseFloat(token) * jackpotData.percentage / 100;
                    var jackpotToken = parseFloat(jackpotData.tokens + jToken);
                    Game.findOneAndUpdate({ slug: 'jackpot' }, { tokens: jackpotToken }, function(error, jackpot) {});
                });


                var finalAmount = parseFloat(req.user.token_bal) - parseFloat(setting.game);
                User.findByIdAndUpdate({ _id: req.user._id }, { 'token_bal': finalAmount }, (error, users) => {
                    res.json(game); // send json data
                });
            }
        })

    } else {
        res.json('0');
    }
});


// View Game history
router.get('/game/history', Middleware.isUser, (req, res, next) => {
    Game.find({}).exec(function(err, gameData) {
        GameHistory.find({ user_id: req.user._id }).sort({ _id: -1 }).exec(function(error, gameHisData) {
            DailyWinner.find({}).sort({ score: 0 }).exec(function(error, dailyWinner) {
                WeeklyWinner.find({}).sort({ score: 0 }).exec(function(error, weeklyWinner) {
                    MonthlyWinner.find({}).sort({ score: 0 }).exec(function(error, monthlyWinner) {
                        JackpotWinner.find({}).sort({ score: 0 }).exec(function(error, jackpotWinner) {
                            res.render('user/gamehistory', { title: 'Game | PaidThru', game: gameData, gameHistory: gameHisData, dailyWinner: dailyWinner, weeklyWinner: weeklyWinner, monthlyWinner: monthlyWinner, jackpotWinner: jackpotWinner, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                        });
                    });
                });
            });
        });
    });
});


// daily
cron.schedule('0 0 0 * * *', function() { // will run every day at 12:00 AM
    Game.findOne({ slug: 'daily' }, function(error, dailyData) {
        GameHistory.findOne({ daily: 0 }).sort({ score: 0 }).exec(function(error, gameHisData) {
            var gameHisId = gameHisData._id;
            var user_id = gameHisData.user_id;
            var user_number = gameHisData.user_number;
            var computer_number = gameHisData.computer_number;
            var score = gameHisData.score;
            User.findByIdAndUpdate({ _id: user_id }, { $inc: { token_bal: dailyData.tokens } }, (error, userData) => {
                const winDaily = createDatabaseWinDaily(dailyData, user_id, user_number, computer_number, score, userData);
                winDaily.save((error, winDaily) => {
                    if (winDaily) {
                        GameHistory.findByIdAndUpdate({ _id: gameHisId }, { daily: 1 }, (error, gmData) => {
                            Game.findOneAndUpdate({ slug: 'daily' }, { tokens: '0' }, function(error, daily) {
                                GameHistory.updateMany({ daily: 0 }, { daily: 2 }, (error, gmDatalost) => {
                                    console.log('daily ok');
                                });
                            });
                        });
                    }
                });
            });
        });
    });
});

// weekly 
cron.schedule('0 0 * * Monday', function() { // will run every monday at 12:00 AM
    Game.findOne({ slug: 'weekly' }, function(error, weeklyData) {
        var firstWin = parseFloat(weeklyData.tokens) * 50 / 100;
        var seccondWin = parseFloat(weeklyData.tokens) * 30 / 100;
        var thirdWin = parseFloat(weeklyData.tokens) * 20 / 100;
        var prizeToken = [firstWin, seccondWin, thirdWin];
        GameHistory.find({ weekly: 0 }).sort({ score: 0 }).limit(3).exec(function(error, gameHisData) {
            var i = 0;
            var j = 1;
            gameHisData.forEach(function(gameHisDatas) {
                var gameHisId = gameHisDatas._id;
                var user_id = gameHisDatas.user_id;
                var user_number = gameHisDatas.user_number;
                var computer_number = gameHisDatas.computer_number;
                var score = gameHisDatas.score;
                var tokenPrize = prizeToken[i++];
                var winner = j++;
                User.findByIdAndUpdate({ _id: user_id }, { $inc: { token_bal: tokenPrize } }, (error, userData) => {
                    const winWeekly = createDatabaseWinWeekly(tokenPrize, user_id, user_number, computer_number, score, userData, winner);
                    winWeekly.save((error, winWeekly) => {
                        if (winWeekly) {
                            GameHistory.findByIdAndUpdate({ _id: gameHisId }, { weekly: 1 }, (error, gmData) => {
                                Game.findOneAndUpdate({ slug: 'weekly' }, { tokens: '0' }, function(error, weekly) {
                                    GameHistory.updateMany({ weekly: 0 }, { weekly: 2 }, (error, gmDatalost) => {
                                        console.log('weekly ok');
                                    });
                                });
                            });
                        }
                    });
                });
            });
        });
    });
});


// monthly
cron.schedule('0 0 1 * *', function() { // will run every month of day 1 at 12:00 AM
    Game.findOne({ slug: 'monthly' }, function(error, monthlyData) {
        var firstWin = parseFloat(monthlyData.tokens) * 50 / 100;
        var seccondWin = parseFloat(monthlyData.tokens) * 30 / 100;
        var thirdWin = parseFloat(monthlyData.tokens) * 20 / 100;
        var prizeToken = [firstWin, seccondWin, thirdWin];
        GameHistory.find({ monthly: 0 }).sort({ score: 0 }).limit(3).exec(function(error, gameHisData) {
            var i = 0;
            var j = 1;
            gameHisData.forEach(function(gameHisDatas) {
                var gameHisId = gameHisDatas._id;
                var user_id = gameHisDatas.user_id;
                var user_number = gameHisDatas.user_number;
                var computer_number = gameHisDatas.computer_number;
                var score = gameHisDatas.score;
                var tokenPrize = prizeToken[i++];
                var winner = j++;
                User.findByIdAndUpdate({ _id: user_id }, { $inc: { token_bal: tokenPrize } }, (error, userData) => {
                    const winMonthly = createDatabaseWinMonthly(tokenPrize, user_id, user_number, computer_number, score, userData, winner);
                    winMonthly.save((error, winMonthly) => {
                        if (winMonthly) {
                            GameHistory.findByIdAndUpdate({ _id: gameHisId }, { monthly: 1 }, (error, gmData) => {
                                Game.findOneAndUpdate({ slug: 'monthly' }, { tokens: '0' }, function(error, monthly) {
                                    GameHistory.updateMany({ monthly: 0 }, { monthly: 2 }, (error, gmDatalost) => {
                                        console.log('monthly ok');
                                    });
                                });
                            });
                        }
                    });
                });
            });
        });
    });
});


// jackpot
cron.schedule('* * * * * *', function() {
    // Game.findOne({ slug: 'jackpot' }, function(error, jackpotData) {
    //     var tokenPrize = jackpotData.tokens;
    //     GameHistory.find({ jackpot: 0 }).exec(function(error, gameHisData) {
    //         gameHisData.forEach(function(gameHisDatas) {
    //             if (gameHisDatas.user_number == gameHisDatas.computer_number) {
    //                 var gameHisId = gameHisDatas._id;
    //                 var user_id = gameHisDatas.user_id;
    //                 var user_number = gameHisDatas.user_number;
    //                 var computer_number = gameHisDatas.computer_number;
    //                 var score = gameHisDatas.score;
    //                 User.findByIdAndUpdate({ _id: user_id }, { $inc: { token_bal: tokenPrize } }, (error, userData) => {
    //                     const winjackpot = createDatabaseWinJackpot(tokenPrize, user_id, user_number, computer_number, score, userData);
    //                     winjackpot.save((error, winjackpot) => {
    //                         if (winjackpot) {
    //                             GameHistory.findByIdAndUpdate({ _id: gameHisId }, { jackpot: 1 }, (error, gmData) => {
    //                                 Game.findOneAndUpdate({ slug: 'jackpot' }, { tokens: '0' }, function(error, jackpot) {});
    //                             });
    //                         }
    //                     });
    //                 });
    //             } else {
    //                 //console.log('not same')
    //             }
    //         });
    //     });
    // });
});



module.exports = router;