var express = require('express');
const router = express.Router();
var speakeasy = require('speakeasy');
var QRCode = require('qrcode');
var Middleware = require('../helpers/middleware');
var User = require('../models/user');


// Creat 2fa
router.get('/create-2fa', Middleware.isUserAdminAllowed, (req, res, next) => {
    var secret = speakeasy.generateSecret({ length: 20, name: 'PaidThru - ' + req.user.email });
    User.findByIdAndUpdate({ _id: req.user._id }, {
        g_2fa: secret.otpauth_url,
        g_2fa_base32: secret.base32
    }, function(err, results) {
        if (err)
            console.log(err)
        else
            QRCode.toDataURL(secret.otpauth_url, function(err, image_data) {
                var arr = {};
                arr.image_url = image_data;
                arr.secret = secret.base32;
                res.send(arr);
            });
    });
});


// Enable 2fa
router.post('/enable-2fa', Middleware.isUserAdminAllowed, (req, res, next) => {

    if (req.body.totp != '') {

        var userToken = req.body.totp;
        var base32secret = req.user.g_2fa_base32;
        var verified = speakeasy.totp.verify({
            secret: base32secret,
            encoding: 'base32',
            token: userToken
        });
        if (verified == true) {
            User.findByIdAndUpdate({ _id: req.user._id }, {
                isset_2fa: 1,
            }, function(err, results) {
                if (err)
                    console.log(err)
                else
                    req.flash('successMessage', 'Google 2FA Enabled Successfully.');
                res.redirect('/profile');
            });
        } else {
            req.flash('errorMessage', 'Google 2FA Code is required.');
            res.redirect('/profile');
        }
    } else {
        req.flash('errorMessage', 'Google 2FA Code is required.');
        res.redirect('/profile');
    }
});


// Disable 2fa
router.post('/disable-2fa', Middleware.isUserAdminAllowed, (req, res, next) => {

    if (req.body.google_2fa_otp != '') {
        var userToken = req.body.google_2fa_otp;
        var base32secret = req.user.g_2fa_base32;
        var verified = speakeasy.totp.verify({
            secret: base32secret,
            encoding: 'base32',
            token: userToken
        });
        if (verified == true) {
            User.findByIdAndUpdate({ _id: req.user._id }, {
                isset_2fa: 0,
                g_2fa: '',
                g_2fa_base32: ''
            }, function(err, results) {
                if (err)
                    console.log(err)
                else
                    req.flash('successMessage', 'Google 2FA Disabled Successfully.');
                res.redirect('/profile');

            });
        } else {
            req.flash('errorMessage', 'Google 2FA Code is required.');
            res.redirect('/profile');
        }
    } else {
        req.flash('errorMessage', 'Google 2FA Code is required.');
        res.redirect('/profile');
    }
});


// Check OTP
router.post('/validate-otp', Middleware.isUserAdminAllowed, (req, res, next) => {
    var userToken = req.body.totp;
    var base32secret = req.user.g_2fa_base32;
    var verified = speakeasy.totp.verify({
        secret: base32secret,
        encoding: 'base32',
        token: userToken
    });
    res.send(verified);
});


/**** Google 2fa validate ******/

router.post('/validate', (req, res, next) => {
    sess = req.session;
    if (req.body.otp_2fa == '') {
        req.flash('errorMessage', 'Please enter OTP First.');
        res.redirect('/validate');
    } else {
        var userToken = req.body.otp_2fa;
        var base32secret = req.user.g_2fa_base32;
        var verified = speakeasy.totp.verify({
            secret: base32secret,
            encoding: 'base32',
            token: userToken
        });
        if (verified == true) {
            sess = req.session;
            sess.is_2fa_otp_validate = 0;
            res.redirect('/dashboard');

        } else {
            req.flash('errorMessage', 'Oops! Invalid OTP.');
            res.redirect('/validate');
        }

    }
});

module.exports = router;