var express = require('express');
const router = express.Router();
var speakeasy = require('speakeasy');
var Middleware = require('../helpers/middleware');
var Email = require('../helpers/emails');
var Paypal = require('../helpers/paypal');
var User = require('../models/user');
var Withdraw = require('../models/withdraw');
var Deposit = require('../models/deposit');
var Coinpayments = require('coinpayments');
var options = require('../config/coinpaymentsConfig.js');
var client = new Coinpayments(options);
var Session = require('express-session');
var sessionInfo;

/********** User Side **********/


// View withdrawal 
router.get('/fiat/:coin', Middleware.isUser, (req, res, next) => {
    var coin = req.params.coin; // coin btc,ltc eth
    Deposit.find({ user_id: user._id, type: coin }).sort({ _id: -1 }).exec(function(err, deposit) {
        res.render('user/fiat-deposit', { title: 'Fiat Deposit | PaidThru', deposit: deposit, coin: coin, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

router.post('/fiat-deposit', Middleware.isUser, (req, res, next) => {
    var paymentID = req.body.paymentID;
    var paymentToken = req.body.paymentToken;
    var orderID = req.body.orderID;
    var payerID = req.body.payerID;
    var amount = req.body.amount;
    var currency = req.body.currency;

    if (req.body.currency == 'EUR') {
        var balance = req.user.eur_bal;
        var coin_bal_name = "eur_bal";
    } else if (req.body.currency == 'USD') {
        var balance = req.user.usd_bal;
        var coin_bal_name = "usd_bal";
    } else if (req.body.currency == 'GBP') {
        var balance = req.user.gbp_bal;
        var coin_bal_name = "gbp_bal";
    } else if (req.body.currency == 'CAD') {
        var balance = req.user.cad_bal;
        var coin_bal_name = "cad_bal";
    }

    var deposit = new Deposit;
    deposit.user_id = req.user._id;
    deposit.amount = amount;
    //deposit.payment_token = paymentToken;
    deposit.txid = paymentID;
    deposit.type = currency;
    deposit.status = 1;
    deposit.users = req.user;
    // save the user
    deposit.save(function(err) {
        if (err) {
            //console.log("err"+err);
            req.flash('error', 'Error in Saving Deposit' + err)
            res.send("error");
        } else {
            var total_amount = parseFloat(balance) + parseFloat(amount);
            // console.log(amount+" "+total_amount);
            User.findByIdAndUpdate({ _id: req.user._id }, {
                [coin_bal_name]: total_amount
            }, function(err, data) {
                if (err) {} else {
                    // console.log(data);
                    res.send("done");
                }
            });
        }
    });
});


router.post('/paynow', Middleware.isUser, (req, res, next) => {
    sessionInfo = req.session;
    sessionInfo.coin = 'usd';
    const data = {
        userID: user._id,
        data: req.body
    }
    /*
     * call to paynow helper method to call paypal sdk
     */
    Paypal.payNow(data, function(error, result) {
        if (error) {
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end(JSON.stringify(error));
        } else {
            sessionInfo.paypalData = result;
            sessionInfo.clientData = req.body;
            res.redirect(result.redirectUrl);
        }
    });

});

/*
 * payment success url 
 */
router.get('/execute', (req, res, next) => {

    sessionInfo = req.session;
    var response = {};
    const PayerID = req.query.PayerID;

    sessionInfo.state = "success";
    Paypal.getResponse(req, PayerID, function(response) {
        res.render('wallet', {
            response: response
        });
    });
});

/*
 * payment cancel url 
 */
router.get('/cancel', (req, res, next) => {
    sessionInfo = req.session;
    console.log(sessionInfo);
    req.flash('errorMessage', 'Payment unsuccessful') // insuffiecient bal
    res.redirect('/wallet/fiat/'.sessionInfo.Session.coin);
});

module.exports = router;