var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var User = require('../models/user');
var Email = require('../helpers/emails');

/********** Admin Side **********/

// Get All user
router.get('/', Middleware.isAdmin, (req, res, next) => {
    User.find({ role: 1, is_deleted: 0 }).sort({ _id: -1 }).exec(function(error, users) {
        if (error)
            console.log('1', error)
        else
            res.render('admin/users', { title: 'Manage Users | PaidThru', users: users, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// active User
router.get('/activate/:id', Middleware.isAdmin, (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, {
        is_active: 1
    }, (error, users) => {
        Email.sendAccountActiveEmail(users);
        res.redirect('/users');
    });
});

// block User
router.get('/block/:id', Middleware.isAdmin, (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, {
        is_active: 0
    }, (error, users) => {
        Email.sendAccountBloackEmail(users);
        res.redirect('/users');
    });
});


// delete User
router.get('/delete/:id', Middleware.isAdmin, (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, {
        is_deleted: 1
    }, (error, users) => {
        res.redirect('/users');
    });
});

// approve User kyc
router.get('/kyc/approve/:id', Middleware.isAdmin, (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, {
        kyc_status: 1
    }, (error, users) => {
        Email.sendKycApprovedEmail(users);
        res.redirect('/users');
    });
});

// Reject User kyc
router.get('/kyc/reject/:id', Middleware.isAdmin, (req, res, next) => {
    User.findByIdAndUpdate({ _id: req.params.id }, {
        kyc_status: -1
    }, (error, users) => {
        Email.sendKycRejectEmail(users);
        res.redirect('/users');
    });
});





module.exports = router;