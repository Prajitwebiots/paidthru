var express = require('express');
const router = express.Router();
var request = require('request');
var cron = require('node-cron');
var Rate = require('../models/rate');
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');


/********** Admin Side **********/

// create rate
const createDatabaseRate = () => new Rate({
    icoStartDate: '23-11-2018',
    icoEndDate: '6-12-2018',
    name: 'Round 5',
    totalSupplyToken: '0',
    rate: '0.001',
    bonus: '5',
    ref_bonus: '15',
});


// create rate database
router.get('/createDatabaseRate', Middleware.isAdmin, (req, res, next) => {
    const rate = createDatabaseRate();
    rate.save((error, rate) => {
        if (error) {
            console.log('1', error);
        } else {
            console.log('Rate successfully created..')
        }
    })
});


// View and Edit Rate
router.get('/', Middleware.isAdmin, (req, res, next) => {
    Rate.find({}).exec(function(err, rateData) {
        res.render('admin/rate', { title: 'Rate Setting | PaidThru', rate: rateData });
    });
});

// Update rate 
router.post('/update', Middleware.isAdmin, (req, res, next) => {
    Rate.findByIdAndUpdate({ _id: req.body.rate_id }, {
        totalSupplyToken: req.body.totalSupplyToken,
        bonus: req.body.bonus,
        icoStartDate: req.body.start_date,
        icoEndDate: req.body.end_date
    }, function(error, rateData) {
        if (error)
            console.log(error)
        else {
            req.flash('success', ' Rate Update Successfully.');
            res.redirect('/rate');
        }
    });
});


module.exports = router;