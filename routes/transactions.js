var express = require('express');
const router = express.Router();
var Middleware = require('../helpers/middleware');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var Deposit = require('../models/deposit');
var Withdraw = require('../models/withdraw');
var BuyToken = require('../models/buytoken');


/********** User Side **********/

// view transaction deposit
router.get('/deposit', Middleware.isUser, (req, res, next) => {
    Deposit.find({ user_id: user._id }).sort({ _id: -1 }).exec(function(err, deposit) {
        res.render('user/transactions/deposit', { title: 'Deposit Transaction | PaidThru', deposit: deposit, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// view transaction withdraw
router.get('/Withdraw', Middleware.isUser, (req, res, next) => {
    Withdraw.find({ user_id: user._id }).sort({ _id: -1 }).exec(function(err, withdraw) {
        res.render('user/transactions/Withdraw', { title: 'Withdraw Transaction | PaidThru', withdraw: withdraw, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// view transaction  buy-token
router.get('/buy-token', Middleware.isUser, (req, res, next) => {
    BuyToken.find({ user_id: user._id }).sort({ _id: -1 }).exec(function(err, buytoken) {
        res.render('user/transactions/buy-token', { title: 'BuyToken Transaction | PaidThru', buytoken: buytoken, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});



/********** Admin Side **********/

// view transaction deposit
router.get('/deposits', Middleware.isAdmin, (req, res, next) => {
    Deposit.find({}).populate('users').sort({ _id: -1 }).exec(function(err, deposit) {
        res.render('admin/transactions/deposit', { title: 'Deposit Transaction | PaidThru', deposit: deposit, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// view transaction withdraw
router.get('/Withdraws', Middleware.isAdmin, (req, res, next) => {
    Withdraw.find({}).populate('users').sort({ _id: -1 }).exec(function(err, withdraw) {
        res.render('admin/transactions/Withdraw', { title: 'Withdraw Transaction | PaidThru', withdraw: withdraw, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});

// view transaction  buy-token
router.get('/buy-tokens', Middleware.isAdmin, (req, res, next) => {
    BuyToken.find({}).populate('users').sort({ _id: -1 }).exec(function(err, buytoken) {
        res.render('admin/transactions/buy-token', { title: 'BuyToken Transaction | PaidThru', buytoken: buytoken, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    });
});


module.exports = router;