var express = require('express');
const router = express.Router();
var passport = require('passport');
var bCrypt = require('bcryptjs');
var async = require('async'); //to avoid dealing with nested callbacks
var crypto = require('crypto'); //generating random token 
var Email = require('../helpers/emails');
var Helper = require('../helpers/helper');
var User = require('../models/user');
var Login = require('../models/login');
var Activation = require('../models/activation');

const LocalStrategy = require('passport-local').Strategy;

// create User
const createDatabaseUser = (body, sponser) => new User({
    username: body.username,
    firstname: body.firstname,
    lastname: body.lastname,
    email: body.email,
    sponser: sponser,
    password: Helper.bcrypt(body.password),
    erc20_address: body.erc20_address,
});

// User activation
const createUserActivation = (body, token) => new Activation({
    user_id: body._id,
    token: token,
    users: body.user,
});

// User Login History
const createLogin = (body, ip, slug) => new Login({
    user_id: body._id,
    ip_address: ip,
    slug: slug,
    users: body.user,
});

// Default route
router.get('/', (req, res, next) => {
    res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});

// lang.. translation
router.post('/change-lang', (req, res, next) => {
    var clang = req.body.lang;
    res.cookie('ulang', clang);
    res.send("done");
    //res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});

// View Register
router.get('/register', (req, res, next) => {
    res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});

// View Login
router.get('/login', (req, res, next) => {
    if (req.user) // if session exists
        res.redirect('/dashboard');
    else
        res.render('auth/login', { 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});

// View Forgot Password
router.get('/forgotPassword', (req, res, next) => {
    res.render('auth/forgotpassword', { 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});

// View Google2fa validate
router.get('/validate', (req, res, next) => {
    res.render('auth/validate', { 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') })
});


// refferal user
router.get('/refer/:user', (req, res, next) => {
    var refUser = req.params.user
    sess = req.session;
    sess.refUser = refUser;
    res.redirect('/register');
});

// Serializing and Deserializing User Instances
passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

/****** Start Signup ********/

// Do Register
router.post('/register', (req, res, next) => {

        const isaddress = Helper.isAddress(req.body.erc20_address);

        req.checkBody('firstname').notEmpty().withMessage('First Name is required');;
        req.checkBody('lastname').notEmpty().withMessage('Last Name is required');
        req.checkBody('email').notEmpty().withMessage('Email is required');
        req.checkBody('username').notEmpty().withMessage('Username is required');
        req.checkBody('password').len(8, 30).withMessage('Min 8 character.').notEmpty().withMessage('Password is required');
        req.checkBody('confirm_password').equals(req.body.password).withMessage('Password dont match').len(8, 30).withMessage('Min 8 character.').notEmpty().withMessage('Confirm Password is required.');
        req.checkBody('erc20_address').notEmpty().withMessage('ERC20 Address is required');

        const error = req.validationErrors();

        if (error) {
            res.render('auth/register', { 'value': req.body, 'error': error, 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
        } else {
            User.findOne({ 'email': req.body.email }, function(err, email) {
                if (email) {
                    req.flash('errorMessage', 'This email address already exist.');
                    res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                } else {

                    if (isaddress == true) {
                        User.findOne({ 'erc20_address': req.body.erc20_address }, function(err, user) {
                            if (user) {
                                req.flash('errorMessage', 'This ERC20 address already exist.');
                                res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                            } else {
                                return next();
                            }
                        });
                    } else {
                        req.flash('errorMessage', 'Please enter valid ERC20 address.');
                        res.render('auth/register', { 'value': req.body, 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
                    }
                }
            });
        }
    },
    passport.authenticate('signup', {
        successRedirect: '/login',
        failureRedirect: '/register',
        failureFlash: true
    }))

passport.use('signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, username, password, done, res) {
    findOrCreateUser = function() {
        User.findOne({ 'username': username }, function(err, user) {
            // already exists
            if (user) {
                return done(null, false, req.flash('errorMessage', 'User already exists'));
            } else {
                sess = req.session;
                var sponser = sess.refUser
                const user = createDatabaseUser(req.body, sponser);
                user.save((error, user) => {
                    if (error) {
                        console.log('1', error);
                    } else {
                        sess = req.session;
                        sess.refUser = "";
                        var token = Helper.randomString(20);
                        Email.sendActivationEmail(user, token);
                        const activation = createUserActivation(user, token);
                        activation.save((error, activation) => {
                            if (error) { console.log('2', error) } else { return done(null, false, req.flash('successMessage', 'Thank you. To complete your registration please check your email.')); }
                        })
                    }
                })
            }
        })
    }
    process.nextTick(findOrCreateUser);
}))

/****** End Signup *******/

/****** Start Login ********/

// Do Login
router.post('/login', (req, res, next) => {
        req.checkBody('email', 'Email is required').notEmpty();
        req.checkBody('password', 'Password is required').notEmpty();
        var error = req.validationErrors();
        if (error) {
            res.render('auth/login', { 'error': error, 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
        } else {
            return next();
        }
    },
    passport.authenticate('login', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true
    }));

passport.use('login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, email, password, done) {
        // check in mongo if a user with username exists or not
        User.findOne({ 'email': email }, function(err, user) {
            if (user) {
                Activation.findOne({ user_id: user._id }, (error, activationExecute) => {

                    if (activationExecute.activate == 0) {
                        return done(null, false, req.flash('errorMessage', 'Your account is not active.'));
                    } else if (user.is_active == 0) {
                        return done(null, false, req.flash('errorMessage', 'Your account is suspended.'));
                    } else if (user.is_deleted == 1) {
                        return done(null, false, req.flash('errorMessage', 'Invalid Email and Password.'));
                    } else {
                        if (user.isset_2fa == 1) {
                            sess = req.session;
                            sess.is_2fa_otp_validate = 1;
                        }
                        // In case of any error, return using the done method
                        if (err) {
                            return done(err);
                        }

                        // Username does not exist, log error & redirect back
                        if (!user) {
                            // user is not in database
                            return done(null, false, req.flash('errorMessage', 'Invalid Email and Password.'));
                        }
                        // User exists but wrong password, log the error 
                        if (!isValidPassword(user, password)) {
                            return done(null, false, req.flash('errorMessage', 'Invalid Email and Password'));
                        }
                        // user or admin role
                        if (user) {
                            if (user.role == 1) {
                                var slug = "user";
                            } else if (user.role == 0) { var slug = "admin"; }
                        }
                        const ip = req.connection.remoteAddress;
                        const loginHistory = createLogin(user, ip, slug);
                        loginHistory.save((error, login) => {
                            if (err)
                                console.log(err);
                            else

                                return done(null, user);
                        })
                    }
                })
            } else {
                return done(null, false, req.flash('errorMessage', 'Invalid Email and Password.'));
            }
        })
    }))

var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
}

/****** End Login ********/

/****** Start Activation User ********/

router.get('/activation/:email/:token', (req, res, next) => {
    User.findOne({ 'email': req.params.email }, function(err, user) {
        if (user) {
            Activation.findOneAndUpdate({ user_id: user._id, 'token': req.params.token }, {
                activate: '1',
            }, function(err, success) {
                if (err) console.log(err);
                else {
                    req.flash('successMessage', 'Your account is activated, Now you can access your account.');
                    res.redirect('/login');
                }
            });
        } else {
            req.flash('errorMessage', 'User is not exists.');
            res.redirect('/login');
        }
    });
})

/****** End Activation User ********/

/****** Start Forgotpassword ********/

router.post('/forgotPassword', (req, res, next) => {
    req.checkBody('email', 'Email is required').notEmpty();
    var error = req.validationErrors();
    if (error) {
        res.render('auth/forgotPassword', { 'error': error, 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage') });
    } else {
        async.waterfall([function(done) {
            crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        }, function(token, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (!user) {
                    req.flash('errorMessage', 'No account with that email address exists.');
                    return res.redirect('/forgotPassword');
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
                user.save(function(err) {
                    Email.sendForgotPassworEmail(user, token);
                    req.flash('successMessage', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                    done(err, token, user);
                });
            });
        }], function(err) {
            if (err) return next(err);
            res.redirect('/forgotPassword');
        });
    }

});

/****** End Forgotpassword ********/

/****** Start Reset Password ********/

// View Reset Password
router.get('/reset/:token', (req, res, next) => {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
            req.flash('errorMessage', 'Password reset token is invalid or has expired.');
            return res.redirect('/forgotPassword');
        }
        res.render('auth/resetPassword', { 'error': req.flash('error'), 'errorMessage': req.flash('errorMessage'), 'successMessage': req.flash('successMessage'), user: req.user, token: req.params.token });
    });
});

router.post('/reset/:token', (req, res, next) => {
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('confirmpassword', 'Confirm Password is required').notEmpty();
    var error = req.validationErrors();
    if (error) {
        req.flash('errorMessage', 'Field are required.');
        return res.redirect('back');
    } else {
        if (req.body.password == req.body.confirmpassword) {
            async.waterfall([
                function(done) {
                    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
                        if (!user) {
                            req.flash('errorMessage', 'Password reset token is invalid or has expired.');
                            return res.redirect('back');
                        }
                        user.password = Helper.bcrypt(req.body.password);
                        user.resetPasswordToken = '';
                        user.resetPasswordExpires = '';
                        user.save(function(err) {
                            req.flash('successMessage', 'Success! Your password has been changed.');
                            res.redirect('/login');
                        });
                    });
                }

            ], function(err) {
                res.redirect('/');
            });
        } else {
            req.flash('errorMessage', 'Password and confirm password do not match');
            return res.redirect('back');
        }
    }

});

/****** End Reset Password ********/




// Logout
router.get('/logout', (req, res, next) => {
    req.logout();
    res.redirect('/login');
});


module.exports = router;