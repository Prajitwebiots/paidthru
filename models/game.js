var mongoose = require('mongoose');

// Game Schema

const GameSchema = mongoose.Schema({
    name: {
        type: String,
    },
    percentage: {
        type: Number,
        default: '0',
    },
    tokens: {
        type: Number,
        default: '0',
    },
    slug: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
});

module.exports = mongoose.model('Game', GameSchema);