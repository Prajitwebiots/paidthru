var mongoose = require('mongoose');

// Rate Schema

const RateSchema = mongoose.Schema({
    icoStartDate: {
        type: String,
    },
    icoEndDate: {
        type: String,
    },
    name: {
        type: String,
    },
    totalSupplyToken: {
        type: Number,
        default: '0',
    },
    rate: {
        type: Number,
        default: '0',
    },
    bonus: {
        type: Number,
        default: '0',
    },
    ref_bonus: {
        type: Number,
        default: '0',
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
});

module.exports = mongoose.model('Rate', RateSchema);