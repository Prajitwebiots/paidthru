var mongoose = require('mongoose');

// User Schema

const UserSchema = mongoose.Schema({
    profile: {
        type: String,
    },
    username: {
        type: String,
        required: true,
        unique: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    sponser: {
        type: String,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    isset_2fa: {
        type: Number,
        default: '0',
    },
    g_2fa: {
        type: String,
    },
    g_2fa_base32: {
        type: String,
    },
    resetPasswordToken: {
        type: String,
    },
    resetPasswordExpires: {
        type: Date,
    },
    token_bal: {
        type: Number,
        default: '0',
    },
    btc_bal: {
        type: Number,
        default: '0',
    },
    eth_bal: {
        type: Number,
        default: '0',
    },
    ltc_bal: {
        type: Number,
        default: '0',
    },
    doge_bal: {
        type: Number,
        default: '0',
    },
    usd_bal: {
        type: Number,
        default: '0',
    },
    eur_bal: {
        type: Number,
        default: '0',
    },
    gbp_bal: {
        type: Number,
        default: '0',
    },
    cad_bal: {
        type: Number,
        default: '0',
    },
    erc20_address: {
        type: String,
        required: true,
    },
    kyc_front: {
        type: String,
    },
    kyc_back: {
        type: String,
    },
    kyc_selfie: {
        type: String,
    },
    kyc_status: {
        type: Number,
        default: '0', // 0-pending | 1-approved | -1 Rejected
    },
    is_active: {
        type: Number,
        default: '1' // 0-block 1-activated
    },
    is_deleted: {
        type: Number,
        default: '0', // 0-present 1-deleted
    },
    role: {
        type: Number,
        default: '1', // 0-admin 1-user
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
});

module.exports = mongoose.model('User', UserSchema);