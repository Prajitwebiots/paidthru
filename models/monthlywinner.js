var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Monthly Winner Schema

const MonthlyWinnerSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    name: {
        type: String,
    },
    user_number: {
        type: Number,
        default: '0',
    },
    computer_number: {
        type: Number,
        default: '0',
    },
    score: {
        type: Number,
        default: '0',
    },
    tokens: {
        type: Number,
        default: '0',
    },
    note: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('MonthlyWinner', MonthlyWinnerSchema);