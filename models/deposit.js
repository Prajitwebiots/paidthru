var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Deposit Schema

const DepositSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    txid: {
        type: String,
    },
    amount: {
        type: Number,
    },
    address: {
        type: String,
    },
    type: {
        type: String, // btc,eth,ltc
    },
    status: {
        type: Number, // 0-pending | 100 confirm | -1 fail
        default: '0',
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('Deposit', DepositSchema);