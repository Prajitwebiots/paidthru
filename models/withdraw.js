var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Withdraw Schema

const WithdrawSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    withdrawalId: {
        type: String,
    },
    txid: {
        type: String,
    },
    amount: {
        type: Number,
    },
    address: {
        type: String,
    },
    type: {
        type: String, // btc,eth,ltc
    },
    status: {
        type: Number, // 0-pending | -1 Pending approval | 1 confirm | 2 cancel | 3 fail | 4 Reject
        default: '0',
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('Withdraw', WithdrawSchema);