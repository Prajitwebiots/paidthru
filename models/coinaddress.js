var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// CoinAddress Schema

const CoinAddressSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    address: {
        type: String,
    },
    type: {
        type: String, // btc,eth,ltc
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('CoinAddress', CoinAddressSchema);