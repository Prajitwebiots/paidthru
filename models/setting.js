var mongoose = require('mongoose');

// Setting Schema

const SettingSchema = mongoose.Schema({
    icoStartDate: {
        type: String,
    },
    icoEndDate: {
        type: String,
    },
    adminEmail: {
        type: String,
    },
    totalSupplyToken: {
        type: Number,
        default: '0',
    },
    soldToken: {
        type: Number,
        default: '0',
    },
    rate: {
        type: Number,
        default: '0',
    },
    bonus: {
        type: Number,
        default: '0',
    },
    ref_bonus: {
        type: Number,
        default: '0',
    },
    btc_price: {
        type: Number,
        default: '0',
    },
    eth_price: {
        type: Number,
        default: '0',
    },
    ltc_price: {
        type: Number,
        default: '0',
    },
    doge_price: {
        type: Number,
        default: '0',
    },
    eur_price: {
        type: Number,
        default: '0',
    },
    gbp_price: {
        type: Number,
        default: '0',
    },
    cad_price: {
        type: Number,
        default: '0',
    },
    game: {
        type: Number,
        default: '0',
    },
    withdrawalStatus: {
        type: Number,
        default: '0', // 0-on | null - off
    },
    slug: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
});

module.exports = mongoose.model('Setting', SettingSchema);