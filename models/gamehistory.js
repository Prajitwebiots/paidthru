var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Game Schema

const GameHistorySchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    user_number: {
        type: Number,
        default: '0',
    },
    computer_number: {
        type: Number,
        default: '0',
    },
    score: {
        type: Number,
        default: '0',
    },
    token: {
        type: Number,
        default: '0',
    },
    daily: {
        type: Number,
        default: '0', // 1 - won
    },
    weekly: {
        type: Number,
        default: '0', // 1 - won
    },
    monthly: {
        type: Number,
        default: '0', // 1 - won
    },
    jackpot: {
        type: Number,
        default: '0', // 1 - won
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('GameHistory', GameHistorySchema);