var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// BuyToken Schema

const BuyTokenSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    txid: {
        type: String,
    },
    amount: {
        type: Number,
    },
    type: {
        type: String, // btc,eth,ltc
    },
    tokens: {
        type: Number, // token ptx
    },
    status: {
        type: Number, // 0-pending | 100 confirm | -1 fail
        default: '0',
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
    users: {
        type: Schema.Types.String,
        ref: 'User'
    }
});

module.exports = mongoose.model('BuyToken', BuyTokenSchema);