var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Activation Schema

const ActivationSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    token: {
        type: String,
    },
    activate: {
        type: Number,
        default: '0', // 0-block 1-activated
    },
    users: {
        type: Schema.Types.String,
        ref: 'User',
    },
    created_date: {
        type: Date,
        default: Date.now, // date now
    },
});

module.exports = mongoose.model('Activation', ActivationSchema);