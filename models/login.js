var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Login Schema

const LoginSchema = mongoose.Schema({
    user_id: {
        type: String,
    },
    ip_address: {
        type: String,
    },
    slug: {
        type: String,
        default: '0',
    },
    users: {
        type: Schema.Types.String,
        ref: 'User',
    },
    created_date: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('Login', LoginSchema);