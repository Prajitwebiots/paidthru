var path = require('path');
var express = require('express');
const app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');
var cron = require('node-cron');
var bodyParser = require('body-parser');
var session = require('express-session');
var config = require('./config/config');
var dbConfig = require('./config/database');
var expressLayouts = require('express-ejs-layouts');
var Game = require('./models/game');
var GameHistory = require('./models/gamehistory');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var i18n = require("i18n-express");
var cookieParser = require('cookie-parser');
global.url = config.baseUrl;

app.use(cookieParser());
//language translation 
app.use(i18n({
    translationsPath: path.join(__dirname, 'lang'), // <--- use here. Specify translations files path.
    cookieLangName: 'ulang',
    browserEnable: true,
    defaultLang: 'english',
    paramLangName: 'clang',
    siteLangs: ["english", "korean", "russian", "spanish", "chinese"],
    textsVarName: 'translation'
}));



// Cron job for game score
cron.schedule('* * * * * *', function() {
    GameHistory.find().sort({ score: 0 }).exec(function(error, gameHisData) {
        Game.findOne({ slug: 'daily' }).exec(function(error, dailyToken) {
            Game.findOne({ slug: 'weekly' }).exec(function(error, weeklyToken) {
                Game.findOne({ slug: 'monthly' }).exec(function(error, monthlyToken) {
                    io.emit('score', gameHisData, dailyToken, weeklyToken, monthlyToken);
                });
            });
        });
    });
});


// Connect to database
mongoose.connect(dbConfig.database);

// On database connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database ' + dbConfig.database);
});

// on databse error
mongoose.connection.on('error', (err) => {
    console.log('Database error ' + err);
});

// Body Parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());

app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(session({ resave: false, saveUninitialized: true, secret: 'node' }));
app.use(flash());

// Set Static Folder
app.use('/public', express.static(path.join(__dirname, 'public')))

var authentication = require('./routes/authentication');
app.use('/', authentication);

// EJS
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout extractScripts', false)
app.set('layout extractStyles', false)
app.use(expressLayouts);


var dashboard = require('./routes/dashboard');
var users = require('./routes/users');
var profile = require('./routes/profile');
var google2fa = require('./routes/google2fa');
var wallet = require('./routes/wallet');
var deposit = require('./routes/deposit');
var withdrawal = require('./routes/withdrawal');
var buyToken = require('./routes/buy-token');
var game = require('./routes/game');
var network = require('./routes/network');
var setting = require('./routes/setting');
var rate = require('./routes/rate');
var transactions = require('./routes/transactions');
var ipn = require('./routes/ipn');
var fiat = require('./routes/fiat');

app.use('/', dashboard);
app.use('/', profile);
app.use('/', google2fa);
app.use('/', buyToken);
app.use('/', game);
app.use('/users', users);
app.use('/wallet', wallet);
app.use('/wallet', deposit);
app.use('/wallet', fiat);
app.use('/wallet', withdrawal);
app.use('/network', network);
app.use('/setting', setting);
app.use('/rate', rate);
app.use('/transaction', transactions);
app.use('/crypto', ipn);


server.listen(config.appPort, function() {
    console.log('Server listening on port ' + config.appPort + '...');
});


// Otherwise navigate to
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'public/errors/404.html'));
});