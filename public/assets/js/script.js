$(document).ready(function() {
    $(".mobile-toggle").click(function() {
        $(".nav-menus").toggleClass("open");
    });
    $(".mobile-search").click(function() {
        $(".form-control-plaintext").toggleClass("open");
    });
});




$(window).on('load', function() {
    $('.loader-wrapper').fadeOut('slow');
    $('.loader-wrapper').remove('slow');
});
$(window).on('scroll', function() {
    if ($(this).scrollTop() > 600) {
        $('.tap-top').fadeIn();
    } else {
        $('.tap-top').fadeOut();
    }
});
$('.tap-top').on('click', function() {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});
$(document).ready(function() {
    if ($(window).width() <= 991) {
        $("#sidebar-toggle").prop('checked', false);
        $(".page-body-wrapper").addClass("sidebar-close");
    }
    $("#sidebar-toggle").change(function() {
        if ($("#sidebar-toggle").attr('checked', true)) {
            $(".page-sidebar").addClass("page-sidebar-open");
        }
    });
});

function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}
(function($, window, document, undefined) {
    "use strict";
    var $ripple = $(".js-ripple");
    $ripple.on("click.ui.ripple", function(e) {
        var $this = $(this);
        var $offset = $this.parent().offset();
        var $circle = $this.find(".c-ripple__circle");
        var x = e.pageX - $offset.left;
        var y = e.pageY - $offset.top;
        $circle.css({
            top: y + "px",
            left: x + "px"
        });
        $this.addClass("is-active");
    });
    $ripple.on(
        "animationend webkitAnimationEnd oanimationend MSAnimationEnd",
        function(e) {
            $(this).removeClass("is-active");
        });
})(jQuery, window, document);


/*
    Copy text from any appropriate field to the clipboard
  By Craig Buckler, @craigbuckler
  use it, abuse it, do whatever you like with it!
*/
(function() {

    'use strict';

    // click events
    document.body.addEventListener('click', copy, true);

    // event handler
    function copy(e) {

        // find target element
        var
            t = e.target,
            c = t.dataset.copytarget,
            inp = (c ? document.querySelector(c) : null);

        // is element selectable?
        if (inp && inp.select) {

            // select text
            inp.select();

            try {
                // copy text
                document.execCommand('copy');
                inp.blur();

                // copied animation
                t.classList.add('copied');
                setTimeout(function() { t.classList.remove('copied'); }, 1500);
            } catch (err) {
                alert('please press Ctrl/Cmd+C to copy');
            }

        }

    }

})();



// datatable

$('.display').DataTable();

// active link
for (var i = 0; i < document.links.length; i++) {
    if (document.links[i].href == document.URL) {
        document.links[i].className += ' active';
        document.links[i].parentElement.parentElement.parentElement.className += ' active';
    }
}

// get method confirmation
$(".confirmation").click(function(e) {
    e.preventDefault();
    var link = $(this).attr('href');
    swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {

            setTimeout(function() {
                window.location.href = link;
            }, 1000);

            swal({
                title: "Success!",
                icon: "success",
                buttons: true,
            });
        }
    });
});


$(".withdrawsReject").click(function(e) {
    e.preventDefault();
    swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {

            setTimeout(function() {
                $('#rejectForm').submit();
            }, 1000);

            swal({
                title: "Success!",
                icon: "success",
                buttons: true,
            });
        }
    });
});

$(".withdrawsApproved").click(function(e) {
    e.preventDefault();
    swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {

            setTimeout(function() {
                $('#approveForm').submit();
            }, 1000);

            swal({
                title: "Success!",
                icon: "success",
                buttons: true,
            });
        }
    });
});


$(".buyTokenButton").click(function(e) {
    e.preventDefault();
    swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {

            setTimeout(function() {
                $('#buyTokenForm').submit();
            }, 1000);

            swal({
                title: "Success!",
                icon: "success",
                buttons: true,
            });
        }
    });
});


setTimeout(function() {
    $('.alert-success').hide();
    $('.alert-danger').hide();
}, 3000);


$(document).ready(function() {

    // var counters = $(".count");
    // var countersQuantity = counters.length;
    // var counter = [];

    // for (i = 0; i < countersQuantity; i++) {
    //     counter[i] = parseInt(counters[i].innerHTML);
    // }

    // var count = function(start, value, id) {
    //     var localStart = start;
    //     setInterval(function() {
    //         if (localStart < value) {
    //             localStart++;
    //             counters[id].innerHTML = localStart;
    //         }
    //     }, 100);
    // }

    // for (j = 0; j < countersQuantity; j++) {
    //     count(0, counter[j], j);
    // }
});